//
//  OpenCVWrapper.m
//  RecognizionIOS
//
//  Created by Camilo Soto on 3/12/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#import <opencv2/imgcodecs/ios.h>
#import <UIKit/UIKit.h>

@implementation OpenCVWrapper

+ (NSString *)openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}

+ (UIImage *)convertToGrayscale:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::Mat gray;
    cv::cvtColor(mat, gray, CV_RGB2GRAY);
    UIImage *grayscale = MatToUIImage(gray);
    return grayscale;
}

+ (UIImage *)cropImageInit:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat);
    int width = image.size.width;
    int height = image.size.height;
    
    int x = width * 0.3;
    int y = height * 0.1;
    int xRect = width - (width * 0.6);
    int yRect = height - (width * 0.5);
    cv::Rect myRoi(x, y, xRect, yRect);
    cv::Mat croppedImage;
    cv::Mat(mat, myRoi).copyTo(croppedImage);
    UIImage *cropImage = MatToUIImage(croppedImage);
    
    return cropImage;
}

@end
