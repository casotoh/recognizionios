//
//  CaptureControllerViewController.swift
//  RecognizionIOS
//
//  Created by Camilo Soto on 4/12/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit
import AVFoundation

class CaptureControllerViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet weak var camPreview: UIView!
    
    let captureSession = AVCaptureSession()
    var image : UIImage? = nil
    var isProcess = true
    var cont = 0
    var takePhoto = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

            captureSession.sessionPreset = .photo
               
            guard let captureDevice = AVCaptureDevice.default(for: .video) else {return}
            guard let input = try? AVCaptureDeviceInput(device: captureDevice) else {return}
            captureSession.addInput(input)
               
            captureSession.startRunning()
               
            let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.frame = camPreview.bounds
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
            camPreview.layer.addSublayer(previewLayer)
               
               
            let dataOutput = AVCaptureVideoDataOutput()
            dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "VideoQueue"))
            captureSession.addOutput(dataOutput)
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //print("La camara fue habilitada para capturar un frame:", Date())
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {return}
        cont += 1
        print("Este es el contador: \(cont) ")
        
        if isProcess && cont > 10{
            captureSession.stopRunning()
            takePhoto = false
            let ciImage : CIImage = CIImage(cvPixelBuffer: pixelBuffer)
            image = self.convert(cmage: ciImage)
            image =  OpenCVWrapper.cropImageInit(image)
            cont = 0
            performSegue(withIdentifier: "toResult", sender: self)
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toResult"{
            let destination = segue.destination as! ViewController
            destination.imageProcess = image
        }
    }
    
    // Convert CIImage to CGImage
    func convert(cmage:CIImage) -> UIImage
    {
         let context:CIContext = CIContext.init(options: nil)
         let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
         var image:UIImage = UIImage.init(cgImage: cgImage)
        image = image.rotate(radians: .pi/2)!
         return image
    }

    @IBAction func takePhoto(_ sender: UIButton) {
        takePhoto = true
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        goBack()
    }
    
    func goBack(){
        //navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}
