//
//  OpenCVWrapper.h
//  RecognizionIOS
//
//  Created by Camilo Soto on 3/12/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface OpenCVWrapper : NSObject

+ (NSString *)openCVVersionString;
+ (UIImage *)convertToGrayscale:(UIImage *)image;
+ (UIImage *)cropImageInit:(UIImage *)image;

@end
