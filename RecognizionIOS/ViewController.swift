//
//  ViewController.swift
//  RecognizionIOS
//
//  Created by Camilo Soto on 3/12/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imgProc: UIImageView!
    @IBOutlet weak var txtResult: UILabel!
    
    
    var imageProcess : UIImage? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
       print("\(OpenCVWrapper.openCVVersionString())")
        
        if imageProcess != nil {
            imgProc.image = imageProcess
            txtResult.text = "OK"
        }else{
            print("La imagen se encuentra Nula")
        }
    }


}

